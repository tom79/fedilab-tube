Added:
- Notification counter
- Mark all notifications as read
- Google: donations via Google Billing

Changed:
- Improve layout for lives
- Move the account item

Fixed:
- Cannot connect Pleroma/Mastodon accounts
- Issue with captions when changing the resolution
- Keep playing when screen is off
- Creating a Playlist without description crashes
- Live not starting issue