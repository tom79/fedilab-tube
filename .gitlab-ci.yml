# This file is a template, and might need editing before it works on your project.
# Read more about this script on this blog post https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/, by Jason Lenny
# If you are interested in using Android with FastLane for publishing take a look at the Android-Fastlane template.

image: openjdk:8-jdk

variables:

  # ANDROID_COMPILE_SDK is the version of Android you're compiling with.
  # It should match compileSdkVersion.
  ANDROID_COMPILE_SDK: "30"

  # ANDROID_BUILD_TOOLS is the version of the Android build tools you are using.
  # It should match buildToolsVersion.
  ANDROID_BUILD_TOOLS: "30.0.2"

  # It's what version of the command line tools we're going to download from the official site.
  # Official Site-> https://developer.android.com/studio/index.html
  # There, look down below at the cli tools only, sdk tools package is of format:
  #        commandlinetools-os_type-ANDROID_SDK_TOOLS_latest.zip
  # when the script was last modified for latest compileSdkVersion, it was which is written down below
  ANDROID_SDK_TOOLS: "6609375"

# Packages installation before running script
before_script:
  - apt-get --quiet update --yes
  - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

  # Setup path as android_home for moving/exporting the downloaded sdk into it
  - export ANDROID_HOME="${PWD}/android-home"
  # Create a new directory at specified location
  - install -d $ANDROID_HOME
  # Here we are installing androidSDK tools from official source,
  # (the key thing here is the url from where you are downloading these sdk tool for command line, so please do note this url pattern there and here as well)
  # after that unzipping those tools and
  # then running a series of SDK manager commands to install necessary android SDK packages that'll allow the app to build
  - wget --output-document=$ANDROID_HOME/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip
  # move to the archive at ANDROID_HOME
  - pushd $ANDROID_HOME
  - unzip -d cmdline-tools cmdline-tools.zip
  - popd
  - export PATH=$PATH:${ANDROID_HOME}/cmdline-tools/tools/bin/

  # Nothing fancy here, just checking sdkManager version
  - sdkmanager --version

  # use yes to accept all licenses
  - yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses || true
  - sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-${ANDROID_COMPILE_SDK}"
  - sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools"
  - sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;${ANDROID_BUILD_TOOLS}"

  # Not necessary, but just for surity
  - chmod +x ./gradle

stages:
  - build
  - build-and-test
  - tag

.no-upload: &no-upload
  stage: build-and-test
  retry: 2

# Make Project
assembleDebug:
  <<: *no-upload
  cache:
    key: "${CI_COMMIT_TAG}"
    paths:
      - app/build/outputs/apk/fdroid_acad/debug/app-fdroid_acad-debug.apk
      - app/build/outputs/apk/fdroid_full/debug/app-fdroid_full-debug.apk
    policy: push
  script:
    - ./gradlew assembleDebug

# Basic android and gradle stuff
# Check linting
lintfdroid_acadDebug:
  interruptible: true
  stage: build
  script:
    - ./gradlew -Pci --console=plain :app:lintfdroid_acadDebug -PbuildDir=lint
  except:
    - tags

lintFdroid_fullDebug:
  interruptible: true
  stage: build
  script:
    - ./gradlew -Pci --console=plain :app:lintFdroid_fullDebug -PbuildDir=lint
  except:
    - tags


## PROTECTED VARIABLES TO SET IN GITLAB:
# - GITLAB_API_TOKEN: token you create on Gitlab
# - NC_REMOTE_DIR:    like https://YOUR_NEXTCLOUD/remote.php/dav/files/YOUR_USER/mastalab (no trailing slash)
# - NC_SHARE_URL:     share the folder in Nextcloud with public link and put your public link here (no trailing slash)
# - NC_USER:          nextcloud user
# - NC_PASSWORD:      nextcloud password
## Protect all tags in Gitlab repo settings (do a wildcard, ie '*')
# For now, it uses the assembleDebug builds, you'll need to create a job in build-and-test to create the apks, with only: - tags and add except: - tags to assembleDebug (like in debugTests)
# In it, put something like this to get your signature key file:
# - curl -s --output signature.jsk -u "${NC_USER}:${NC_PASSWORD}" "https://YOUR_NEXTCLOUD/remote.php/dav/files/YOUR_USER/signature.jsk"
putApkOnTags:
  image: hatsoftwares/curl-jq:latest
  stage: tag
  retry: 2
  cache:
    key: "${CI_COMMIT_TAG}"
    paths:
      - app/build/outputs/apk/fdroid_acad/debug/app-fdroid_acad-debug.apk
      - app/build/outputs/apk/fdroid_full/debug/app-fdroid_full-debug.apk
    policy: pull
  script:
    - export PROJECT_API_URL="https://framagit.org/api/v4/projects/${CI_PROJECT_ID}"
    - export DESCRIPTION_URL="${PROJECT_API_URL}/repository/tags/${CI_COMMIT_TAG}"
    - export RELEASE_URL="${DESCRIPTION_URL}/release"
    - export NC_UPLOAD_URL="${NC_REMOTE_DIR}/${CI_COMMIT_TAG}"
    - export NC_DOWNLOAD_URL="${NC_SHARE_URL}/download?path=%2F${CI_COMMIT_TAG}%2F&files="
    - 'export HEADER="Private-Token: ${GITLAB_API_TOKEN}"'
    - export acadUrl="${NC_DOWNLOAD_URL}app-fdroid_acad-debug.apk"
    - export fullUrl="${NC_DOWNLOAD_URL}app-fdroid_full-debug.apk"
    - 'curl -s -u "${NC_USER}:${NC_PASSWORD}" -X MKCOL "${NC_UPLOAD_URL}"'
    - 'curl -s -u "${NC_USER}:${NC_PASSWORD}" -T app/build/outputs/apk/fdroid_full/debug/app-fdroid_full-debug.apk       "${NC_UPLOAD_URL}/app-fdroid_full-debug.apk"'
    - 'curl -s -u "${NC_USER}:${NC_PASSWORD}" -T app/build/outputs/apk/fdroid_acad/debug/app-fdroid_acad-debug.apk "${NC_UPLOAD_URL}/app-fdroid_acad-debug.apk"'
    - export description=$(curl -s --header "${HEADER}" "${DESCRIPTION_URL}" | jq .release.description | sed -e 's@"@@g')
    - if [[ $description == 'null' ]]; then export METHOD="POST"; echo -e "[Get the acad version](${acadUrl})\n\n[Get the full version](${fullUrl})" > /tmp/text; fi
    - if [[ $description != 'null' ]]; then export METHOD="PUT";  echo -e "${description}\n\n[Get the acad version](${acadUrl})\n\n[Get the full version](${fullUrl})" > /tmp/text; fi
    - curl -s --request $METHOD --data-urlencode "description@/tmp/text" --header "${HEADER}" "${RELEASE_URL}"
  only:
    - tags
