<?xml version="1.0" encoding="utf-8"?><!--
    Copyright 2020 Thomas Schneider

    This file is a part of TubeLab

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation; either version 3 of the
    License, or (at your option) any later version.

    TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along with TubeLab; if not,
    see <http://www.gnu.org/licenses>.
-->
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_margin="@dimen/fab_margin"
        android:orientation="vertical">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center"
            android:gravity="center"
            android:orientation="horizontal">

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center"
                android:gravity="center"
                android:textSize="18sp" />

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center"
                android:layout_marginStart="10dp"
                android:gravity="center" />
        </LinearLayout>


        <TextView
            android:id="@+id/error_message"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center"
            android:layout_margin="10dp"
            android:background="@drawable/red_border"
            android:gravity="center"
            android:padding="5dp"
            android:textColor="@color/red_1"
            android:visibility="gone" />


        <LinearLayout
            android:visibility="gone"
            android:id="@+id/title_login_instance"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="20dp"
            android:orientation="horizontal">

            <com.google.android.material.textfield.TextInputLayout
                android:id="@+id/login_instance_container"
                android:layout_width="0dp"
                android:layout_weight="1"
                android:layout_height="wrap_content"
                app:layout_constraintEnd_toEndOf="parent">

                <com.google.android.material.textfield.TextInputEditText
                    android:id="@+id/login_instance"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:inputType="textWebEditText"
                    android:hint="@string/title_instance_login"
                    android:singleLine="true"
                    android:importantForAutofill="no" />

            </com.google.android.material.textfield.TextInputLayout>

            <Button
                android:id="@+id/instance_help"
                style="@style/Base.Widget.AppCompat.Button.Colored"
                android:layout_width="wrap_content"
                android:layout_height="35dp"
                android:layout_gravity="center_vertical"
                android:padding="2dp"
                android:text="@string/help" />
        </LinearLayout>


        <com.google.android.material.textfield.TextInputLayout
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:errorEnabled="true">

            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/username"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/username"
                android:maxLength="50"
                android:importantForAutofill="no"
                android:inputType="text" />
        </com.google.android.material.textfield.TextInputLayout>


        <com.google.android.material.textfield.TextInputLayout
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:errorEnabled="true">

            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/email"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/email"
                android:importantForAutofill="no"
                android:inputType="textEmailAddress" />
        </com.google.android.material.textfield.TextInputLayout>

        <TextView
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/email_indicator"
            android:textSize="12sp" />


        <com.google.android.material.textfield.TextInputLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:errorEnabled="true">

            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/password"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:inputType="textPassword"
                android:hint="@string/password"
                android:importantForAutofill="no" />
        </com.google.android.material.textfield.TextInputLayout>


        <TextView
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/password_indicator"
            android:textSize="12sp" />

        <com.google.android.material.textfield.TextInputLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:errorEnabled="true">

            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/password_confirm"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/password_confirm"
                android:inputType="textPassword"
                android:importantForAutofill="no" />
        </com.google.android.material.textfield.TextInputLayout>

        <LinearLayout
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="horizontal">

            <CheckBox
                android:id="@+id/agreement"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content" />

            <TextView
                android:id="@+id/agreement_text"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginBottom="10dp"
                android:text="@string/agreement_check_peertube" />
        </LinearLayout>

        <Button
            android:layout_marginTop="10dp"
            android:id="@+id/signup"
            style="@style/Base.Widget.AppCompat.Button.Colored"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/sign_up" />
    </LinearLayout>
</ScrollView>
