package app.fedilab.fedilabtube.fragment;
/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;

import java.util.HashMap;
import java.util.List;

import app.fedilab.fedilabtube.databinding.FragmentMyDonationsBinding;
import app.fedilab.fedilabtube.drawable.DonationHistoryAdapter;

public class MySubscriptionDonationsFragment extends Fragment {

    private FragmentMyDonationsBinding binding;
    private View rootView;
    private Context context;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMyDonationsBinding.inflate(LayoutInflater.from(context));
        rootView = binding.getRoot();
        context = getContext();
        binding.loader.setVisibility(View.VISIBLE);
        binding.lvPurchases.setVisibility(View.GONE);
        return rootView;
    }

    public void initialized(List<SkuDetails> skuDetailsList, HashMap<String, Purchase> map, BillingClient bc) {
        binding.loader.setVisibility(View.GONE);
        binding.lvPurchases.setVisibility(View.VISIBLE);

        DonationHistoryAdapter donationHistoryAdapter = new DonationHistoryAdapter(skuDetailsList, map, bc);
        binding.lvPurchases.setAdapter(donationHistoryAdapter);
        binding.lvPurchases.setLayoutManager(new LinearLayoutManager(context));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
    }

    @Override
    public void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
