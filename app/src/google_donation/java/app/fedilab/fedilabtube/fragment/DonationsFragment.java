package app.fedilab.fedilabtube.fragment;
/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetailsParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.fedilab.fedilabtube.R;
import app.fedilab.fedilabtube.databinding.FragmentDonationsBinding;
import app.fedilab.fedilabtube.drawable.DonationButtonAdapter;


public class DonationsFragment extends Fragment {

    public static final String[] donations = {"1", "2", "5", "10"};
    private FragmentDonationsBinding binding;
    private View rootView;
    private Context context;
    private boolean isSubscriptions;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentDonationsBinding.inflate(LayoutInflater.from(context));
        rootView = binding.getRoot();
        context = getContext();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isSubscriptions = bundle.getBoolean("isSubscriptions", false);
        }
        int donationText;
        if (isSubscriptions) {
            donationText = R.string.recurrent_donation_text;
        } else {
            donationText = R.string.one_time_donation_text;
        }
        binding.donationText.setText(donationText);
        binding.loader.setVisibility(View.VISIBLE);
        binding.lvProducts.setVisibility(View.GONE);
        return rootView;
    }

    public void initialized(BillingClient bc) {


        List<String> donationsList = new ArrayList<>();
        for (String val : donations) {
            donationsList.add("tubelab_donation_" + val + (isSubscriptions ? "_s" : ""));
        }
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        if (isSubscriptions) {
            params.setSkusList(donationsList).setType(BillingClient.SkuType.SUBS);
        } else {
            params.setSkusList(donationsList).setType(BillingClient.SkuType.INAPP);
        }
        bc.querySkuDetailsAsync(params.build(),
                (billingResult, skuDetailsList) -> {
                    binding.loader.setVisibility(View.GONE);
                    binding.lvProducts.setVisibility(View.VISIBLE);
                    if (skuDetailsList != null) {
                        Collections.sort(skuDetailsList, (obj1, obj2) -> obj1.getPrice().compareTo(obj2.getPrice()));
                    }
                    DonationButtonAdapter donationButtonAdapter = new DonationButtonAdapter(skuDetailsList, bc, isSubscriptions);
                    binding.lvProducts.setAdapter(donationButtonAdapter);
                    binding.lvProducts.setLayoutManager(new LinearLayoutManager(context));
                });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rootView = null;
    }

    @Override
    public void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
